//
//  Birthdaycake.h
//  CakeCutter
//
//  Created by Enrico Susatyo on 4/06/11.
//  Copyright 2011 Enrico Susatyo. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const int MAX_PEOPLE;

@interface Birthdaycake : UIView {
    NSString *shape;
    int numberOfPeople;
    BOOL useDegree;
}

-(void) increase;
-(void) decrease;
-(void) setPeople:(int)p;
-(NSString*) getDescription;
-(NSString*) getCircleDescription;
-(NSString*) getRectangleDescription;

@property (nonatomic, strong) NSString* shape;
@property int numberOfPeople;
@property BOOL useDegree;

-(BOOL)isPrime:(int)n;
-(int)highestPrime:(int)p;

@end

