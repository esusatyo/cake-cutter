//
//  main.m
//  CakeCutter
//
//  Created by Enrico Susatyo on 4/06/11.
//  Copyright 2011 Enrico Susatyo. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, nil);
        return retVal;
    }
}
