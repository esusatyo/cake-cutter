//
//  CakeCutterViewController.h
//  CakeCutter
//
//  Created by Enrico Susatyo on 4/06/11.
//  Copyright 2011 Enrico Susatyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Birthdaycake.h"

@interface CakeCutterViewController : UIViewController {
    IBOutlet UISegmentedControl *shapeChooser;
//    IBOutlet UIButton *increaseButton;
//    IBOutlet UIButton *decreaseButton;
    IBOutlet UILabel *numberOfPeopleLabel;
    IBOutlet UILabel *cuttingInstructionLabel;
    IBOutlet Birthdaycake* birthdayCake;
    __unsafe_unretained IBOutlet UISlider *slider;
}

//-(IBAction) increaseNumberOfPeople:(id)sender;
//-(IBAction) decreaseNumberOfPeople:(id)sender;
-(IBAction) didChangeShape:(id)sender;
-(void) updateUserInterface;
- (IBAction)didChangeSlider:(id)sender;
- (IBAction)degreeSwitchFlipped:(id)sender;

@end
