//
//  CakeCutterViewController.m
//  CakeCutter
//
//  Created by Enrico Susatyo on 4/06/11.
//  Copyright 2011 Enrico Susatyo. All rights reserved.
//

#import "CakeCutterViewController.h"

@implementation CakeCutterViewController

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [birthdayCake setNumberOfPeople:5];
    [birthdayCake setShape:@"Circle"];
    [birthdayCake setUseDegree:YES];
    
    [self updateUserInterface];
    
    [super viewDidLoad];
}


- (void)viewDidUnload
{
    slider = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - IBAction functions

/*
-(IBAction) increaseNumberOfPeople:(id)sender
{
    [birthdayCake increase];
    [self updateUserInterface];
}

-(IBAction) decreaseNumberOfPeople:(id)sender
{
    [birthdayCake decrease];
    [self updateUserInterface];
}
*/

-(IBAction) didChangeShape:(id)sender
{
    if (shapeChooser.selectedSegmentIndex == 0) {
        birthdayCake.shape = @"Rectangle";
    }
    else {
        birthdayCake.shape = @"Circle";
    }
    [self updateUserInterface];
}

-(void) updateUserInterface 
{    
    //Update labels
    numberOfPeopleLabel.text = [NSString stringWithFormat:@"%d people", birthdayCake.numberOfPeople];
    cuttingInstructionLabel.text = [birthdayCake getDescription];
    
    //Update the cake display
    [birthdayCake setNeedsDisplay];
}

- (IBAction)didChangeSlider:(id)sender {
    float sliderValue = [slider value]/[slider maximumValue];
    int people = sliderValue*MAX_PEOPLE;
    NSLog(@"value now is %d", people);
    [birthdayCake setPeople:people];
    [self updateUserInterface];
}

- (IBAction)degreeSwitchFlipped:(id)sender {
    birthdayCake.useDegree = !birthdayCake.useDegree;
    [self updateUserInterface];
}

@end
