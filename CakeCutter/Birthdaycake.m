//
//  Birthdaycake.m
//  CakeCutter
//
//  Created by Enrico Susatyo on 4/06/11.
//  Copyright 2011 Enrico Susatyo. All rights reserved.
//

#import "Birthdaycake.h"


@implementation Birthdaycake

@synthesize shape;
@synthesize numberOfPeople;
@synthesize useDegree;

const int MAX_PEOPLE = 26;

- (id)initWithFrame:(CGRect)frame
{    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    int margin = 2.0;
    CGFloat centerX = (self.frame.size.width-(margin*2))/2.0;
    CGFloat centerY = (self.frame.size.height-(margin*2))/2.0;
    CGFloat radius = centerX; //radius can be either centerX or Y
    
    CGContextRef context = UIGraphicsGetCurrentContext();    
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, [UIColor darkGrayColor].CGColor);
    
    CGRect area = CGRectMake(margin, margin, self.frame.size.width-margin*2, self.frame.size.height-margin*2);
    
    // draw a circle
    if([shape isEqualToString:@"Circle"])
    {
        CGContextAddEllipseInRect(context, area);
        CGContextStrokePath(context);

        double totalDegree = 360.0;
        double theta = totalDegree/numberOfPeople;
        
        if(numberOfPeople > 1) {
            for(int i = 0; i < numberOfPeople; i++)
            {
                theta = fmod(theta + (360.0/numberOfPeople), 360.0);
                CGFloat x = centerX + radius * (cos(theta * (M_PI /180.0)));
                CGFloat y = centerY + radius * (sin(theta * (M_PI /180.0)));
                    
                CGContextMoveToPoint(context, centerX, centerY);
                CGContextAddLineToPoint(context, x, y);
                CGContextStrokePath(context);
            }
        }
        
    }
    //draw a rectangle
    else
    {
        CGContextAddRect(context, area);
        CGContextStrokePath(context);
        
        int highestPrime = [self highestPrime:numberOfPeople];
        
        if(highestPrime < numberOfPeople)
        {
            int numberOfVertical = numberOfPeople/highestPrime;
            for (int lines = 0; lines < numberOfVertical; lines++) {
                CGFloat x = margin + (lines* (((self.frame.size.width-margin)/numberOfVertical)) );
                CGFloat y = margin;
                
                CGContextMoveToPoint(context, x, y);
                CGContextAddLineToPoint(context, x, y + (self.frame.size.width-margin));
            }
            
            int numberOfHorizontal = highestPrime;
            for (int lines = 0; lines < numberOfHorizontal; lines++) {
                CGFloat y = margin + (lines* (((self.frame.size.width-margin)/numberOfHorizontal)) );
                CGFloat x = margin;
                
                CGContextMoveToPoint(context, x, y);
                CGContextAddLineToPoint(context, x + (self.frame.size.width-margin), y);
            }
            CGContextStrokePath(context);
        }
        //prime numbers, cut with width/people
        else
        {
            for (int lines = 0; lines < numberOfPeople; lines++) {
                CGFloat x = margin + (lines* (((self.frame.size.width-margin)/numberOfPeople)) );
                CGFloat y = margin;
                
                CGContextMoveToPoint(context, x, y);
                CGContextAddLineToPoint(context, x, y + (self.frame.size.width-margin));
            }
            CGContextStrokePath(context);
        }
        
    }

}



-(void) increase
{
    if(numberOfPeople < MAX_PEOPLE) 
        numberOfPeople++;
}

-(void) decrease
{
    if(numberOfPeople > 1) 
        numberOfPeople--;
}

-(void) setPeople:(int)p
{
    if(p < MAX_PEOPLE && p > 1)
        numberOfPeople = p;
}

-(NSString*) getDescription
{
    if([shape isEqualToString:@"Circle"])
        return [self getCircleDescription];
    else
        return [self getRectangleDescription];
}

-(NSString*) getCircleDescription
{
    if(useDegree) {
        return [NSString stringWithFormat:@"Cut your cake at %d degree", 360/numberOfPeople];
    }
    else {
        NSString *radian = [NSString stringWithFormat: @"2/%d",numberOfPeople];
        if(numberOfPeople%2 == 0)
            radian = [NSString stringWithFormat:@"1/%d", (numberOfPeople/2)];
            
        return [NSString stringWithFormat:@"Cut your cake at %@ π",radian];
    }
}

-(NSString*) getRectangleDescription
{
    return @"Cut your cake as shown above";
}

-(int)highestPrime:(int)p
{
    int factor = p;
    while (factor > 1) {
        if([self isPrime:factor] && (p%factor == 0))
            return factor;
        factor--;
    }
    return factor;
}

-(BOOL)isPrime:(int)n
{
    if (n < 4) return YES;
    for(int i = 2; i*i <= n; i++)
    {
        if(n%i == 0)
            return NO;
    }
    return YES;
}

@end
