//
//  CakeCutterAppDelegate.h
//  CakeCutter
//
//  Created by Enrico Susatyo on 4/06/11.
//  Copyright 2011 Enrico Susatyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CakeCutterViewController;

@interface CakeCutterAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, strong) IBOutlet UIWindow *window;

@property (nonatomic, strong) IBOutlet CakeCutterViewController *viewController;

@end
